# README

## Visão Geral

Uma brincadeira para aprender a programar em JavaScript (JS) e o uso de algum framework que facilite a minha vida de aprendiz em programação JS. Além disso, tentarei fazer desse um *app* útil sendo um projeto para realizar consultas ao WebService do Sistema de Transferência de Arquivos do Banco Central do Brasil. Assim, será nomeado como **Bacen-STAWS** para fácil identicação e rápida referência.

A construção se dará a partir de informações disponíveis no site do [Banco Central do Brasil][1] (Bacen) e seguirá as regras de acesso e segurança lá indicadas.

## Licenciamento

O produto desse projeto segue a licença disponível no arquivo [LICENSE](<https://bitbucket.org/adriano_svieira/bacen-staws/src/b1d53d2428a80625f39e0578298ed92d985783ca/LICENSE?at=master>).

## Estrutura do repositório

```
README.md            Este arquivo que explica a estrutura do projeto/repositório
TODO.md              Apresenta o projeto e lista suas necessidades
```

## Referências

[1]: http://www.bcb.gov.br/?TRANSFARQ        "SISBACEN » Sistema de transferência de arquivos"
*SISBACEN » Sistema de transferência de arquivos*. Bacen. Banco Central do Brasil. Disponível em: <http://www.bcb.gov.br/?TRANSFARQ>. Acessado em: junho, 2014.