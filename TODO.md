# TODO

Lista necessidades para consultas ao WebService Bacen-STAWS

## Requisitos

Define os requisitos de negócio para a consulta ao serviço web.

1. possibilitar registro de usuário de consulta e senha  
  o login de usuário a ser usado na requisição será no formato ```UUUUUDDDD.operador```, onde:
   - ```UUUUU```: Código Sisbacen da sua instituição
   - ```DDDD```: Código Sisbacen da sua dependência
   - ```operador```: Seu nome de usuário
1. permitir registro de HOST_URL (dominio/webservice) - (HTTPS)  
  operador e senha devem ser específicos para cada HOST_URL
   - homologação (ex: ```https://sta-h.bcb.gov.br/staws```)
   - produção (ex: ```https://sta.bcb.gov.br/staws```)
1. realizar teste de conectividade  
   - ```https://HOST_URL/arquivos?tipoConsulta=AVANC&nivelDetalhe=RES```
1. permitir consultas de situação de arquivos
   1. Teste de conectividade:
      - ```https://HOST_URL/arquivos?tipoConsulta=AVANC&nivelDetalhe=RES```  
      - ```https://HOST_URL/arquivos?tipoConsulta=AVANC&nivelDetalhe=BAS```
   1. Consulta por protocolo
      - ```https://HOST_URL/arquivos?tipoConsulta=PROT&nivelDetalhe=BAS&protocolos=73097096```
   1. Consulta por datahora
      - ```https://HOST_URL/arquivos/disponiveis?dataHoraInicio=2014-04-29T12:00:00.000```
      - ```https://HOST_URL/arquivos/disponiveis?dataHoraInicio=2014-05-23T00:00:01.000```
1. permitir que haja um HOST_URL favorito
   esse recurso será usado para que alertas desse favorito seja mostrado
1. gerar alerta caso algum envio tenha ocorrido problema
1. gerar alerta caso algum recebimento tenha ocorrido problema
